import { Component, ViewEncapsulation, Inject, OnInit, OnDestroy } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { StorageService } from './services/storage.service';

import { faMoon as fasMoon} from '@fortawesome/free-solid-svg-icons';
import { faMoon as farMoon } from '@fortawesome/free-regular-svg-icons';
import { RxjsDisposables } from './utils/rxjs.utils';
import { filter, tap } from 'rxjs/operators';


const DEVICE_COLOR_THEME = 'DEVICE_COLOR_THEME';

const enum ColorThemesEnum {
  light,
  dark
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class AppComponent implements OnInit, OnDestroy {

  private rx = new RxjsDisposables()

  fasMoon = fasMoon
  farMoon = farMoon
  selectedTheme: ColorThemesEnum;

  constructor(@Inject(DOCUMENT) private document: Document, private store: StorageService) {}



  ngOnInit() {
    this.initTheme();
    this.rx.add = this.store.changes$.pipe(
      filter( change => change.key === DEVICE_COLOR_THEME),
      tap( change => this.applyColorThemeClass(change.value.data, this.document.body))
    ).subscribe();
    
  }
  ngOnDestroy(): void {
    this.rx.dispose();
  }






  initTheme() {
    const storedDeviceColorTheme = this.store.get<ColorThemesEnum>(DEVICE_COLOR_THEME)?.data;
    if(storedDeviceColorTheme) {
      this.applyColorThemeClass(storedDeviceColorTheme,  this.document.body);
    } else {
      const browserPreferedTheme = deviceColorPrefrance();
      this.store.set(DEVICE_COLOR_THEME, browserPreferedTheme)
      this.applyColorThemeClass(browserPreferedTheme,  this.document.body);
      
    }
  }

  onToggleTheme() {
    let currentTheme = this.store.get<ColorThemesEnum>(DEVICE_COLOR_THEME)?.data
    if(!currentTheme) { currentTheme = ColorThemesEnum.light}
    console.log(currentTheme)
    currentTheme === ColorThemesEnum.light ? this.applyColorThemeClass(ColorThemesEnum.dark,  this.document.body) : this.applyColorThemeClass(ColorThemesEnum.light,  this.document.body)
    currentTheme === ColorThemesEnum.light ? this.store.set(DEVICE_COLOR_THEME, ColorThemesEnum.dark) : this.store.set(DEVICE_COLOR_THEME, ColorThemesEnum.light)

  }



  applyColorThemeClass(theme:ColorThemesEnum, body: HTMLElement) {
    
    if(theme === ColorThemesEnum.dark ) {
      this.selectedTheme = ColorThemesEnum.dark;
      body.classList.remove('light-mode');
      body.classList.add('dark-mode');
    } else if (theme === ColorThemesEnum.light) {
      this.selectedTheme = ColorThemesEnum.light;
      body.classList.remove('dark-mode');
      body.classList.add('light-mode');
    }
  }




}


function deviceColorPrefrance(): ColorThemesEnum {
  return window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches ? ColorThemesEnum.dark : ColorThemesEnum.light;
}
