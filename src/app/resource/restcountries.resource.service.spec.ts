import { TestBed } from '@angular/core/testing';

import { RestCountriesResource } from './restcountries.resource.service';

describe('RestcountriesResource', () => {
  let service: RestCountriesResource;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RestCountriesResource);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
