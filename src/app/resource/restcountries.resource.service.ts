import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, iif, of, from, Observable } from 'rxjs';
import { mergeMap, tap, filter, switchMap, first, catchError, shareReplay, take, toArray, map } from 'rxjs/operators';
import { RestCountriesResourceDTO } from './restcountries';
import { StorageService } from '../services/storage.service';


const REST_COUNTRIES = "REST_COUNTRIES"

@Injectable({
  providedIn: 'root'
})
export class RestCountriesResource {

  constructor(private http:HttpClient, private storage:StorageService) { }



private _Countries$: Observable<RestCountriesResourceDTO[]>
get countries$(): Observable<RestCountriesResourceDTO[]> {
  const cache = this.storage.get<RestCountriesResourceDTO[]>(REST_COUNTRIES);
  if(cache) {
    this._Countries$ = new Observable<RestCountriesResourceDTO[]>( (obs)  => obs.next(cache.data)).pipe(shareReplay(1));
  } else if(!this._Countries$) {
    this._Countries$ = this.http.get<RestCountriesResourceDTO[]>("https://restcountries.eu/rest/v2/all").pipe(
      tap( res => this.storage.set(REST_COUNTRIES, res)),
      shareReplay(1)
    );
  }

  return this._Countries$
}





private _countrysStore = new Map<String, RestCountriesResourceDTO>()
getCountry$(countryCode:string): Observable<RestCountriesResourceDTO> {
  const cache = this._countrysStore.get(countryCode);
  let obs: Observable<RestCountriesResourceDTO>;

  if (cache) {
    return of(cache);
  } else if( this._Countries$ ) {
    obs = this.countries$.pipe(
      switchMap(country => from(country)),
      filter( country => country.alpha2Code === countryCode || country.alpha3Code === countryCode ),
      first(),
    );
  } else {
    obs = this.fetchCountryByName(countryCode)
  }

  return obs.pipe(
    tap( res => this._countrysStore.set(countryCode, res) )
  )
  
}

private fetchCountryByName(countryCode:string): Observable<RestCountriesResourceDTO> {
  return this.http.get<RestCountriesResourceDTO>(`https://restcountries.eu/rest/v2/alpha/${countryCode}`)
  .pipe( 
    tap( res => console.log(res)),
  )
}


private regions = ['Africa', 'Americas', 'Asia', 'Europe', 'Oceania'];
getRegions() {
  return this.regions
}

}

// https://restcountries.eu/rest/v2/name/aruba?fullText=true
// this._data.getValue().length 
// return this.data$.pipe( 
//   switchMap(country => from(country)),
//   filter( country => country.name === countryName || country.nativeName === countryName ),
//   first()
//   )