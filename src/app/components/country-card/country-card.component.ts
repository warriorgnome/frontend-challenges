import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

export interface CountryCardState {
  imageUrl: string;
  name: string;

  data: {
    population: number;
    region: string
    capital: string
  }
  countryCode: string
}


@Component({
  selector: 'country-card',
  templateUrl: './country-card.component.html',
  styleUrls: ['./country-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    // class: "col-12 col-lg-3 element m-2 my-4 p-0 r-1 z-1 hover-z-1",
  }
})
export class CountryCardComponent implements OnInit {



  @Input() data: CountryCardState;

  constructor() { }

  ngOnInit(): void {
  }

}
