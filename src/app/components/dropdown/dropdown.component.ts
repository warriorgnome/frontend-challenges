import { Component, OnInit, Input } from '@angular/core';
import { RxjsDisposables } from 'src/app/utils/rxjs.utils';
import { fromEvent } from 'rxjs';
import { filter, tap } from 'rxjs/operators';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {

  @Input() shadow = true;

  private rx = new RxjsDisposables();

  constructor() { }
  
  ngOnInit(): void {
    this.rx.add = fromEvent(window, 'click').pipe(
      // tap(e => console.log('fromEvent', e)),
      filter(e => this.isOpen && !!this.elRef),
      // tap(e => console.log('fromEvent', e)),
      tap(e => this.close())
    ).subscribe() 
  }


  private elRef: HTMLElement;
  private isOpen = false;



  toggle(el: HTMLElement, event?:Event) {
    event?.stopPropagation();
    // console.log(e, event);
    el.classList.toggle('open');
    if(!this.elRef) { this.elRef = el; }
    this.isOpen =  !this.isOpen
  }

  open(el: HTMLElement) {
    el.classList.add('open');
    if(!this.elRef) { this.elRef = el; }
    this.isOpen =  true
  }

  close() {
    this.elRef.classList.remove('open');
    this.isOpen =  false
  }



  keyboardControllWrapper(el: HTMLElement, event:KeyboardEvent) {
    console.log(event.keyCode);
    if( event.keyCode === Keys.TAB) {
      return
    }
    // event.preventDefault();
    // event.stopPropagation();
    if(keyArray.includes(event.keyCode)) {
      if( (event.target as HMTLSlotElement).slot === 'button' ) {
        this.dropdownButtonHandler(el, event)
      } else {
        this.contentsButtonHandler(el, event)

      }

    }
    
    // if(event.keyCode === Keys.DOWN) {

    //   console.log(el);
    //   console.log(el.firstElementChild);
    //   console.log(getFirstFocusable(el.firstElementChild));
    //   getFirstFocusable(el.firstElementChild).focus()
      // this.elRef.firstElementChild.focus()
    // }

  }


  dropdownButtonHandler(el: HTMLElement, event:KeyboardEvent) {
    if(event.keyCode === Keys.DOWN || event.keyCode === Keys.ENTER) {
      console.log(el);

      this.open(el)
      getFirstFocusable(el.firstElementChild).focus()

      // console.log(el.firstElementChild);
      // console.log(getFirstFocusable(el.firstElementChild));
      // this.elRef.firstElementChild.focus()
    }

  }

  contentsButtonHandler(el: HTMLElement, event:KeyboardEvent) {
      console.log(el);

    if(event.keyCode === Keys.DOWN) {
      ((event.target as HTMLElement).nextElementSibling as FocusableHTMLElement)?.focus()
    } else if (event.keyCode === Keys.UP) {
      ((event.target as HTMLElement).previousElementSibling as FocusableHTMLElement)?.focus()
    } else if( event.keyCode === Keys.ENTER) {
      (event.target as HTMLElement).click()
    } else if( event.keyCode === Keys.ESCAPE) {
      this.close()
    }

  }

  log(args) {
    console.log( ...args)
  }
}


export const keyArray: number[] = [37, 38, 39, 40, 13, 27, 32]


export const enum Keys {
    "TAB" = 9,
    "LEFT" = 37,
    "UP" = 38,
    "RIGTH" = 39,
    "DOWN" = 40,
    "ENTER" = 13,
    "ESCAPE" = 27,
    "SPACE" = 32,
}


function getFirstFocusable(el: Element, first=true): FocusableHTMLElement {
    if(first && el.querySelector('#first')) {
      return el.querySelector('#first');
    }

  if( el.firstElementChild ) {
    if( (el.firstElementChild as FocusableHTMLElement).focus ) {
      return el.firstElementChild as FocusableHTMLElement;
    } else {
      getFirstFocusable(el.firstElementChild, false);
    }

  }
  return null;
}


export interface FocusableHTMLElement  extends HTMLElement{
  focus(): () => void | undefined;
}

export interface HMTLSlotElement  extends HTMLElement{
    slot: string
}