import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { RestCountriesResource } from 'src/app/resource/restcountries.resource.service';
import { map, switchMap, tap, shareReplay, share, filter, mergeMap, combineLatest } from 'rxjs/operators';
import { of, Observable, Subject, ReplaySubject, BehaviorSubject } from 'rxjs';
import { CountryCardState } from 'src/app/components/country-card/country-card.component';
import { RestCountriesResourceDTO } from 'src/app/resource/restcountries';
import { faSearch, faChevronDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CountriesComponent implements OnInit {

  faSearch = faSearch
  faChevronDown = faChevronDown

  constructor(private resource: RestCountriesResource) { }
  activeRegionFilter$ = new BehaviorSubject<string>(null);
  regions = this.resource.getRegions();

  countries$ = this.resource.countries$.pipe(
    tap( res => console.log('countries$', res)),
    map( countries =>   transformDTOtoCountryCardState(countries) ),
    combineLatest(this.activeRegionFilter$),
    map( ([countries, filter]) => {
      if (filter) {
        console.log('123');
        return countries.filter( country => country.data.region === filter)
      } else {
        console.log('456');
        return countries
      }
    })
  );

  

  ngOnInit(): void {
  }
  log(e) {
    console.log(e);
    
  }



  setFilterByRegion(val: string) {
    console.log('1+82947891274+8712+', val);
    
    // if(!this.activeRegionFilter$) {
    //   this.activeRegionFilter$ = new BehaviorSubject<string>(1)
    // }
    this.activeRegionFilter$.next(val)
  }

}


function transformDTOtoCountryCardState(countries: RestCountriesResourceDTO[]): CountryCardState[] {
  return countries.map( country => {
    return { 
      name: country.name, imageUrl: country.flag,
      data: {
        capital: country.capital,
        population: country.population,
        region: country.region,
      },
      countryCode: country.alpha3Code
    
    }
  })
}
