import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RxjsDisposables } from 'src/app/utils/rxjs.utils';
import { tap, map, mergeMap } from 'rxjs/operators';
import { RestCountriesResource } from 'src/app/resource/restcountries.resource.service';
import { faLongArrowAltLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent implements OnInit {

  faLongArrowAltLeft = faLongArrowAltLeft

  rx = new RxjsDisposables()
  constructor(private router: ActivatedRoute, private resouce: RestCountriesResource ) { }

  ngOnInit(): void {
  }


  pageContext$ = this.router.params.pipe(
    tap( params => console.log(params) ),
    map( params => params.countryName ),
    mergeMap( name => this.resouce.getCountry$(name))
    
  )
}
