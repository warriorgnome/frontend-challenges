import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CountriesComponent } from './pages/countries/countries.component';
import { CountryCardComponent } from './components/country-card/country-card.component';
import { TypeofPipe } from './pipes/typeof.pipe';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CountryComponent } from './pages/country/country.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';




const ROUTES: Routes = [
  { path: "", component: CountriesComponent},
  { path: ":countryName", component: CountryComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    CountriesComponent,
    CountryCardComponent,
    TypeofPipe,
    CountryComponent,
    DropdownComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES),
    FontAwesomeModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
