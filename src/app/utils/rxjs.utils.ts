import { Subscription } from 'rxjs';




export class RxjsDisposables  {
    private subscriptions:Subscription[] = [];


    set add(val: Subscription) {
        this.subscriptions.push(val);
    }


    dispose() {
        this.subscriptions.forEach( sub => sub.unsubscribe())
    }

}