import { Injectable, OnDestroy } from '@angular/core';
import { Subject, fromEvent } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RxjsDisposables } from '../utils/rxjs.utils';

export interface StorageChange {
  type: 'set' | 'remove' | 'change'
  key: string
  value?: StorageObject
}

export interface StorageObject<T = any> {
  timeStamp: Date
  data: T
}

@Injectable({
  providedIn: 'root'
})
export class StorageService implements OnDestroy {
  private rx = new RxjsDisposables();
  private PREFIX = 'rest-countries';
  private localStorage: Storage;
  
  changes$ = new Subject<StorageChange>();


  constructor() {
    this.localStorage = window.localStorage;
    if(this.isLocalStorageSupported) {
      // event is fired if a change is done in another window/tab
      this.rx.add = fromEvent<StorageEvent>(window, 'storage').pipe(
        tap( change => {
          const key = change.key.includes(this.PREFIX) ? change.key.slice(this.PREFIX.length) : change.key;
          this.changes$.next({ type: 'change', key, value: JSON.parse(change.newValue)})
        })
      ).subscribe();
    } else {
      console.warn('LocalStorage is not supported on this device')
    }    
   }


  set(key:string, value:any): boolean {
    if (this.isLocalStorageSupported) {
      this.localStorage.setItem(this.PREFIX + key, JSON.stringify({ timeStamp: new Date, data:value}));
      this.changes$.next({ type: 'set', key, value});
      return true;
    }
    return false;
  }

  get<T = any>(key:string): StorageObject<T> {
    if (this.isLocalStorageSupported) {
      console.log( 'get', JSON.parse(this.localStorage.getItem(this.PREFIX +key)) );
      
      return JSON.parse(this.localStorage.getItem(this.PREFIX +key));
    }
    return null;
  } 

  remove(key: string): boolean {
    if (this.isLocalStorageSupported) {
      this.localStorage.removeItem(this.PREFIX + key);
      this.changes$.next({ type: 'remove', key});
      return true;
    }
    return false;
  }







  private get isLocalStorageSupported(): boolean {
    return !!this.localStorage
  }



  ngOnDestroy(): void {
    this.rx.dispose();
  }
}
