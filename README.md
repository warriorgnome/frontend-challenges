
Used: 

 - Angular 9
 - @fortawesome
 - bootstrap [ grid system ]

Implemented

 - cross window cache of main api.
 - dual color themes, brigth and dark.
 - cross window sync of chosen color theme and cache selected prefrance.
 
Missing

 - Searing by text, tinking about adding Fuse.js.
 
Noteworthy

 - RxjsDisposables: new and simple way to collect Subsciptions and unsubcribe.
 - StorageService, with sync cross windows and timestamp if service/resouces want to implement cache invalidation.
 - a11y dropdown component.


#### Goal
https://www.frontendmentor.io/challenges

![alternativetext](Challenge/design/desktoppreview.jpg)
